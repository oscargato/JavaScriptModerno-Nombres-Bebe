document.querySelector('#generar-nombre').addEventListener('submit',cargaNombres);

//Llamado a Ajax e imprimir resultados
function  cargaNombres(e){
	e.preventDefault();

	//Leer las variables
	const origen = document.getElementById('origen');
	const origenSeleccionado = origen.options[origen.selectedIndex].value;

	const genero = document.getElementById('genero');
	const generoSeleccionado = genero.options[genero.selectedIndex].value;

	const cantidad = document.getElementById('numero').value;

	let url='http://uinames.com/api/?';

	//Si hay origen agregarlo a la URL
	if(origenSeleccionado !== ''){
		url += `region=${origenSeleccionado}&`;
	}

	//Si hay una cantidad agregarlo a la URL
	if(generoSeleccionado !== ''){
		url += `gender=${generoSeleccionado}&`;
	}

	//Si hay una cantidad agregarlo a la URL
	if(cantidad !== ''){
		url += `amount=${cantidad}&`;
	}

	//Conectar con Ajax
	const xhr = new XMLHttpRequest();
	xhr.open('GET',url,true);
	xhr.onload = function(){
		if(this.status === 200){
			const nombres = JSON.parse(this.responseText);
			let htmlNombres = '<h2>Nombres Generados</h2>';
			htmlNombres += '<ul class="lista">';
				nombres.forEach( function(nombre){
					htmlNombres += `
						<li>${nombre.name}</li>
					`;	
				});
			htmlNombres += '</ul>'

			document.getElementById('resultado').innerHTML = htmlNombres;
		}
	}  
	xhr.send();
}